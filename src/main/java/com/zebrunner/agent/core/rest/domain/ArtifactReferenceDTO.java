package com.zebrunner.agent.core.rest.domain;

import lombok.Value;

@Value
public class ArtifactReferenceDTO {

    String name;
    String reference;

}
